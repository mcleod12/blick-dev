
if ($('.js-hero_slick')[0]){
    $('.js-hero_slick').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        speed: 500,
        pauseOnFocus:false,
    });
}


if($('.js-product-carousel_slick')[0]){
    $('.js-product-carousel_slick').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
                breakpoint: 1028,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '24%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },

            {
                breakpoint: 375,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '20%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]
    });
}

if($('#instagram')[0]){
    var accessToken = '388254498.1a8e535.064cc19452b843a4acf4d1d214e88417';
    var num_photos  = 7;
    var feed        = $('#instagram');

    $.ajax({
        type: 'GET',
        url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken + '&count=' + num_photos,
        dataType: 'json',
        success: function(result){

            var content = "";
            $.each(result.data, function(i){
                content += i <= 2 || i > 5 ? '<li class="instagram__feed-item">' : '';
                content += i == 2 ? '<ul class="instagram__feed-item__grid">' : '';
                content += i >= 2 && i <= 5 ? '<li class="instagram__feed-item__grid-item">' : '';
                content += ('<a target="_blank" class="instagram__feed-link" href="'+result.data[i].link+'" style="background-image:url('+result.data[i].images.standard_resolution.url+');"></a>');
                content += i >= 2 && i <= 5 ? "</li>" : "";
                content += i == 5 ? "</ul>" : "";
                content += i > 5 ? "</li>" : "";
            });

            feed.append(content);
        }
    });

}





















































































